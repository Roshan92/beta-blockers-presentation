$(document).ready(function () {

    var vid = document.getElementById("myVideo");
    let slidesLength = slidesData['data'].length;
    var delay = 0;
    var image = 0;
    var timer;
    var pauseMe = false;
    for (let index = 0; index < slidesLength; index++) {
        $('.custom-thumbnail').append(
            `
            <div class="card-list">
              <div class="card-image">
                  <a class="change-me" href="#">
                    <p>${slidesData['data'][index].slideTitle}</p>
                  </a><input type="hidden" value="${index}">
              </div>
              <div class="card-content">
                <p hidden>${slidesData['data'][index].inTime}</p>
              </div>
            <div>
        `
        );
    }

    $('video').on('play', function (e) {
        timer = setTimeout(function changeImage() {
            if (slidesLength === image) {
                clearTimeout(timer);
            } else {
                // console.log(vid.currentTime);
                document.querySelector('#showImage').src = `img/${slidesData['data'][image].slideNo}.png`;
                console.log(slidesData['data'][image].slideNo);
                if (image === 0) {
                    delay = slidesData['data'][image].inTime * 1000;
                } else {
                    if((image + 1) >= slidesLength){
                        delay = slidesData['data'][image].inTime * 1000;
                    }else{
                        delay = parseFloat((slidesData['data'][image + 1].inTime * 1000)) - parseFloat((slidesData['data'][image].inTime * 1000));
                    }
                }
                console.log(image + " : " + delay);
                image += 1;

                timer = setTimeout(changeImage, delay);
            }

        }, delay);
    });

    $('video').on('pause', function (e) {
        clearTimeout(timer);
        pauseMe = true;
    });

    $('.card-list').on('click', '.change-me', function (e) {
        // let slide = $(this).siblings('img').attr('src');
        if (image <= slidesLength) {
            image = parseInt($(this).siblings('input').val());
            document.querySelector('#showImage').src = `img/${slidesData['data'][image].slideNo}.png`;
            vid.currentTime = parseFloat($(this).parent('div').siblings().children().text());
            delay = parseFloat((slidesData['data'][image + 1].inTime * 1000)) - parseFloat((slidesData['data'][image].inTime * 1000));
            vid.play();
        }

    });

    document.querySelector('video').addEventListener('timeupdate', function (e) {
        for (let i = 0; i < slidesLength; i++) {
            if((i + 1) > slidesLength){
                document.querySelector('#showImage').src = `img/${slidesData['data'][i].slideNo}.png`;

                    image = i;
                    delay =  parseFloat((slidesData['data'][i].inTime * 1000));
            }else{
                if (vid.currentTime >= slidesData['data'][i].inTime && vid.currentTime < slidesData['data'][i + 1].inTime) {
                    // console.log("Current Time : ", vid.currentTime, " Current Slide Time : ", slidesData['data'][image].inTime, " Next Slide Time: ", slidesData['data'][image + 1].inTime);
                    document.querySelector('#showImage').src = `img/${slidesData['data'][i].slideNo}.png`;

                    image = i;
                    delay = parseFloat((slidesData['data'][i + 1].inTime * 1000)) - parseFloat((slidesData['data'][i].inTime * 1000));
                }
            }
        }

    });

    function convertTime(jsontime) {
        let fullTime = jsontime.split(':');
        console.log()
    }
});
