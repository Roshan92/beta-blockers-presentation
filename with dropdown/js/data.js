const slidesData = {
 data: [
   {
       'slideNo': 'Slide01',
       'slideTitle': 'Beta-blockers in the treatment of hypertension associated with sympathetic overdrive',
       'inTime': 0.00000
   },
   {
       'slideNo': 'Slide02',
       'slideTitle': 'What is sympathetic overdrive?',
       'inTime': 20.07680
   },
   {
       'slideNo': 'Slide03',
       'slideTitle': '',
       'inTime': 22.46080
   },
   {
       'slideNo': 'Slide04',
       'slideTitle': '',
       'inTime': 34.28800
   },
   {
       'slideNo': 'Slide05',
       'slideTitle': '',
       'inTime': 51.30720
   },
   {
       'slideNo': 'Slide06',
       'slideTitle': '',
       'inTime': 57.34560
   },
   {
       'slideNo': 'Slide07',
       'slideTitle': '',
       'inTime': 1 * 60 + 55.24960
   },
   {
       'slideNo': 'Slide08',
       'slideTitle': '',
       'inTime': 2 * 60 + 17.34560
   },
   {
       'slideNo': 'Slide09',
       'slideTitle': '',
       'inTime': 2 * 60 + 50.36480
   },
   {
       'slideNo': 'Slide10',
       'slideTitle': 'Heart rate as an independent cardiovascular risk factor',
       'inTime': 3 * 60 + 14.24960
   },
   {
       'slideNo': 'Slide11',
       'slideTitle': '',
       'inTime': 3 * 60 + 36.40320
   },
   {
       'slideNo': 'Slide12',
       'slideTitle': '',
       'inTime': 4 * 60 + 11.07680
   },
   {
       'slideNo': 'Slide13',
       'slideTitle': '',
       'inTime': 4 * 60 + 46.28800
   },
   {
       'slideNo': 'Slide14',
       'slideTitle': '',
       'inTime': 5 * 60 + 19.26880
   },
   {
       'slideNo': 'Slide15',
       'slideTitle': '',
       'inTime': 5 * 60 + 29.40320
   },
   {
       'slideNo': 'Slide16',
       'slideTitle': '',
       'inTime': 5 * 60 + 50.03840
   },
   {
       'slideNo': 'Slide17',
       'slideTitle': '',
       'inTime': 6 * 60 + 12.42240
   },
   {
       'slideNo': 'Slide18',
       'slideTitle': '',
       'inTime': 6 * 60 + 39.46080
   },
   {
       'slideNo': 'Slide19',
       'slideTitle': 'Sympathetic overdrive is common at initial stage of hypertension',
       'inTime': 7 * 60 + 3.01920
   },
   {
       'slideNo': 'Slide20',
       'slideTitle': '',
       'inTime': 7 * 60 + 48.30720
   },
   {
       'slideNo': 'Slide21',
       'slideTitle': '',
       'inTime': 8 * 60 + 57.03840
   },
   {
       'slideNo': 'Slide22',
       'slideTitle': '',
       'inTime': 10 * 60 + 6.01920
   },
   {
       'slideNo': 'Slide23',
       'slideTitle': '',
       'inTime': 11 * 60 + 24.34560
   },
   {
       'slideNo': 'Slide24',
       'slideTitle': '',
       'inTime': 12 * 60 + 36.03840
   },
   {
       'slideNo': 'Slide25',
       'slideTitle': '',
       'inTime': 12 * 60 + 49.17280
   },
   {
       'slideNo': 'Slide26',
       'slideTitle': '',
       'inTime': 14 * 60 + 30.21120
   },
   {
       'slideNo': 'Slide27',
       'slideTitle': '',
       'inTime': 15 * 60 + 53.34560
   },
   {
       'slideNo': 'Slide28',
       'slideTitle': '',
       'inTime': 17 * 60 + 52.23040
   },
   {
       'slideNo': 'Slide29',
       'slideTitle': 'Treatment of hypertension: What guidelines say?',
       'inTime': 18 * 60 + 37.17280
   },
   {
       'slideNo': 'Slide30',
       'slideTitle': '',
       'inTime': 19 * 60 + 8.32640
   },
   {
       'slideNo': 'Slide31',
       'slideTitle': '',
       'inTime': 19 * 60 + 21.17280
   },
   {
       'slideNo': 'Slide32',
       'slideTitle': '',
       'inTime': 20 * 60 + 4.19200
   },
   {
       'slideNo': 'Slide33',
       'slideTitle': '',
       'inTime': 20 * 60 + 17.26880
   },
   {
       'slideNo': 'Slide34',
       'slideTitle': '',
       'inTime': 20 * 60 + 34.11520
   },
   {
       'slideNo': 'Slide35',
       'slideTitle': '',
       'inTime': 20 * 60 + 54.28800
   },
   {
       'slideNo': 'Slide36',
       'slideTitle': '',
       'inTime': 22 * 60 + 7.07680
   },
   {
       'slideNo': 'Slide37',
       'slideTitle': '',
       'inTime': 22 * 60 + 17.23040
   },
   {
       'slideNo': 'Slide38',
       'slideTitle': '',
       'inTime': 22 * 60 + 24.26880
   },
   {
       'slideNo': 'Slide39',
       'slideTitle': 'Differences between Beta-blockers',
       'inTime': 22 * 60 + 54.46080
   },
   {
       'slideNo': 'Slide40',
       'slideTitle': '',
       'inTime': 23 * 60 + 4.21120
   },
   {
       'slideNo': 'Slide41',
       'slideTitle': '',
       'inTime': 23 * 60 + 31.30720
   },
   {
       'slideNo': 'Slide42',
       'slideTitle': '',
       'inTime': 24 * 60 + 49.46080
   },
   {
       'slideNo': 'Slide43',
       'slideTitle': '',
       'inTime': 25 * 60 + 30.32640
   },
   {
       'slideNo': 'Slide44',
       'slideTitle': '',
       'inTime': 26 * 60 + 47.19200
   },
   {
       'slideNo': 'Slide45',
       'slideTitle': '',
       'inTime': 27 * 60 + 12.05760
   },
   {
       'slideNo': 'Slide46',
       'slideTitle': 'Beta-blockers in the treatment of hypertension',
       'inTime': 27 * 60 + 51.13440
   },
   {
       'slideNo': 'Slide47',
       'slideTitle': '',
       'inTime': 27 * 60 + 56.42240
   },
   {
       'slideNo': 'Slide48',
       'slideTitle': '',
       'inTime': 29 * 60 + 33.23040
   },
   {
       'slideNo': 'Slide49',
       'slideTitle': '',
       'inTime': 30 * 60 + 4.17280
   },
   {
       'slideNo': 'Slide50',
       'slideTitle': '',
       'inTime': 31 * 60 + 7.44160
   },
   {
       'slideNo': 'Slide51',
       'slideTitle': '',
       'inTime': 32 * 60 + 5.15360
   },
   {
       'slideNo': 'Slide52',
       'slideTitle': '',
       'inTime': 33 * 60 + 56.07680
   },
   {
       'slideNo': 'Slide53',
       'slideTitle': 'Beta-selectivity: Efficacy with low risk of adverse events',
       'inTime': 35 * 60 + 32.11520
   },
   {
       'slideNo': 'Slide54',
       'slideTitle': '',
       'inTime': 35 * 60 + 51.32640
   },
   {
       'slideNo': 'Slide55',
       'slideTitle': '',
       'inTime': 36 * 60 + 34.03840
   },
   {
       'slideNo': 'Slide56',
       'slideTitle': '',
       'inTime': 37 * 60 + 24.24960
   },
   {
       'slideNo': 'Slide57',
       'slideTitle': '',
       'inTime': 38 * 60 + 38.38400
   },
   {
       'slideNo': 'Slide58',
       'slideTitle': 'Drug combinations in the treatment of hypertension',
       'inTime': 39 * 60 + 7.15360
   },
   {
       'slideNo': 'Slide59',
       'slideTitle': '',
       'inTime': 39 * 60 + 46.38400
   },
   {
       'slideNo': 'Slide60',
       'slideTitle': '',
       'inTime': 40 * 60 + 34.23040
   },
   {
       'slideNo': 'Slide61',
       'slideTitle': '',
       'inTime': 41 * 60 + 30.34560
   },
   {
       'slideNo': 'Slide62',
       'slideTitle': '',
       'inTime': 43 * 60 + 0.42240
   },
   {
       'slideNo': 'Slide63',
       'slideTitle': '',
       'inTime': 43 * 60 + 30.09600
   },
   {
       'slideNo': 'Slide64',
       'slideTitle': '',
       'inTime': 43 * 60 +51.38400
   },
   {
       'slideNo': 'Slide65',
       'slideTitle': '',
       'inTime': 44 * 60 + 43.03840
   },
   {
       'slideNo': 'Slide66',
       'slideTitle': '',
       'inTime': 45 * 60 + 4.17280
   },
   {
       'slideNo': 'Slide67',
       'slideTitle': '',
       'inTime': 45 * 60 + 59.23040
   },
   {
       'slideNo': 'Slide68',
       'slideTitle': '',
       'inTime': 47 * 60 + 18.38400
   },
   {
       'slideNo': 'Slide69',
       'slideTitle': '',
       'inTime': 47* 60 + 50.05760
   },
   {
       'slideNo': 'Slide70',
       'slideTitle': '',
       'inTime': 48 * 60 + 9.09600
   }
 ]
};

// const slidesData = {
//     data: [
//         {
//             'slideNo': 'Slide01',
//             'inTime': 0.00000
//         },
//         {
//             'slideNo': 'Slide02',
//             'inTime': 20.07680
//         },
//         {
//             'slideNo': 'Slide03',
//             'inTime': 22.46080
//         },
//         {
//             'slideNo': 'Slide04',
//             'inTime': 34.28800
//         },
//         {
//             'slideNo': 'Slide05',
//             'inTime': 51.30720
//         },
//         {
//             'slideNo': 'Slide06',
//             'inTime': 57.34560
//         },
//         {
//             'slideNo': 'Slide07',
//             'inTime': 1 * 60 + 55.24960
//         },
//         {
//             'slideNo': 'Slide08',
//             'inTime': 2 * 60 + 17.34560
//         },
//         {
//             'slideNo': 'Slide09',
//             'inTime': 2 * 60 + 50.36480
//         },
//         {
//             'slideNo': 'Slide10',
//             'inTime': 3 * 60 + 14.24960
//         },
//         {
//             'slideNo': 'Slide11',
//             'inTime': 3 * 60 + 36.40320
//         },
//         {
//             'slideNo': 'Slide12',
//             'inTime': 4 * 60 + 11.07680
//         },
//         {
//             'slideNo': 'Slide13',
//             'inTime': 4 * 60 + 46.28800
//         },
//         {
//             'slideNo': 'Slide14',
//             'inTime': 5 * 60 + 19.26880
//         },
//         {
//             'slideNo': 'Slide15',
//             'inTime': 5 * 60 + 29.40320
//         },
//         {
//             'slideNo': 'Slide16',
//             'inTime': 5 * 60 + 50.03840
//         },
//         {
//             'slideNo': 'Slide17',
//             'inTime': 6 * 60 + 12.42240
//         },
//         {
//             'slideNo': 'Slide18',
//             'inTime': 6 * 60 + 39.46080
//         },
//         {
//             'slideNo': 'Slide19',
//             'inTime': 7 * 60 + 3.01920
//         },
//         {
//             'slideNo': 'Slide20',
//             'inTime': 7 * 60 + 48.30720
//         },
//         {
//             'slideNo': 'Slide21',
//             'inTime': 8 * 60 + 57.03840
//         },
//         {
//             'slideNo': 'Slide22',
//             'inTime': 10 * 60 + 6.01920
//         },
//         {
//             'slideNo': 'Slide23',
//             'inTime': 11 * 60 + 24.34560
//         },
//         {
//             'slideNo': 'Slide24',
//             'inTime': 12 * 60 + 36.03840
//         },
//         {
//             'slideNo': 'Slide25',
//             'inTime': 12 * 60 + 49.17280
//         },
//         {
//             'slideNo': 'Slide26',
//             'inTime': 14 * 60 + 30.21120
//         },
//         {
//             'slideNo': 'Slide27',
//             'inTime': 15 * 60 + 53.34560
//         },
//         {
//             'slideNo': 'Slide28',
//             'inTime': 17 * 60 + 52.23040
//         },
//         {
//             'slideNo': 'Slide29',
//             'inTime': 18 * 60 + 37.17280
//         },
//         {
//             'slideNo': 'Slide30',
//             'inTime': 19 * 60 + 8.32640
//         },
//         {
//             'slideNo': 'Slide31',
//             'inTime': 19 * 60 + 21.17280
//         },
//         {
//             'slideNo': 'Slide32',
//             'inTime': 20 * 60 + 4.19200
//         },
//         {
//             'slideNo': 'Slide33',
//             'inTime': 20 * 60 + 17.26880
//         },
//         {
//             'slideNo': 'Slide34',
//             'inTime': 20 * 60 + 34.11520
//         },
//         {
//             'slideNo': 'Slide35',
//             'inTime': 20 * 60 + 54.28800
//         },
//         {
//             'slideNo': 'Slide36',
//             'inTime': 22 * 60 + 7.07680
//         },
//         {
//             'slideNo': 'Slide37',
//             'inTime': 22 * 60 + 17.23040
//         },
//         {
//             'slideNo': 'Slide38',
//             'inTime': 22 * 60 + 24.26880
//         },
//         {
//             'slideNo': 'Slide39',
//             'inTime': 22 * 60 + 54.46080
//         },
//         {
//             'slideNo': 'Slide40',
//             'inTime': 23 * 60 + 4.21120
//         },
//         {
//             'slideNo': 'Slide41',
//             'inTime': 23 * 60 + 31.30720
//         },
//         {
//             'slideNo': 'Slide42',
//             'inTime': 24 * 60 + 49.46080
//         },
//         {
//             'slideNo': 'Slide43',
//             'inTime': 25 * 60 + 30.32640
//         },
//         {
//             'slideNo': 'Slide44',
//             'inTime': 26 * 60 + 47.19200
//         },
//         {
//             'slideNo': 'Slide45',
//             'inTime': 27 * 60 + 12.05760
//         },
//         {
//             'slideNo': 'Slide46',
//             'inTime': 27 * 60 + 51.13440
//         },
//         {
//             'slideNo': 'Slide47',
//             'inTime': 27 * 60 + 56.42240
//         },
//         {
//             'slideNo': 'Slide48',
//             'inTime': 29 * 60 + 33.23040
//         },
//         {
//             'slideNo': 'Slide49',
//             'inTime': 30 * 60 + 4.17280
//         },
//         {
//             'slideNo': 'Slide50',
//             'inTime': 31 * 60 + 7.44160
//         },
//         {
//             'slideNo': 'Slide51',
//             'inTime': 32 * 60 + 5.15360
//         },
//         {
//             'slideNo': 'Slide52',
//             'inTime': 33 * 60 + 56.07680
//         },
//         {
//             'slideNo': 'Slide53',
//             'inTime': 35 * 60 + 32.11520
//         },
//         {
//             'slideNo': 'Slide54',
//             'inTime': 35 * 60 + 51.32640
//         },
//         {
//             'slideNo': 'Slide55',
//             'inTime': 36 * 60 + 34.03840
//         },
//         {
//             'slideNo': 'Slide56',
//             'inTime': 37 * 60 + 24.24960
//         },
//         {
//             'slideNo': 'Slide57',
//             'inTime': 38 * 60 + 38.38400
//         },
//         {
//             'slideNo': 'Slide58',
//             'inTime': 39 * 60 + 7.15360
//         },
//         {
//             'slideNo': 'Slide59',
//             'inTime': 39 * 60 + 46.38400
//         },
//         {
//             'slideNo': 'Slide60',
//             'inTime': 40 * 60 + 34.23040
//         },
//         {
//             'slideNo': 'Slide61',
//             'inTime': 41 * 60 + 30.34560
//         },
//         {
//             'slideNo': 'Slide62',
//             'inTime': 43 * 60 + 0.42240
//         },
//         {
//             'slideNo': 'Slide63',
//             'inTime': 43 * 60 + 30.09600
//         },
//         {
//             'slideNo': 'Slide64',
//             'inTime': 43 * 60 +51.38400
//         },
//         {
//             'slideNo': 'Slide65',
//             'inTime': 44 * 60 + 43.03840
//         },
//         {
//             'slideNo': 'Slide66',
//             'inTime': 45 * 60 + 4.17280
//         },
//         {
//             'slideNo': 'Slide67',
//             'inTime': 45 * 60 + 59.23040
//         },
//         {
//             'slideNo': 'Slide68',
//             'inTime': 47 * 60 + 18.38400
//         },
//         {
//             'slideNo': 'Slide69',
//             'inTime': 47* 60 + 50.05760
//         },
//         {
//             'slideNo': 'Slide70',
//             'inTime': 48 * 60 + 9.09600
//         }
//     ]
// };
